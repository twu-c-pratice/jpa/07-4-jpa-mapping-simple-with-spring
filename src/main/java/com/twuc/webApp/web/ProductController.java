package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ProductController {
    @Autowired private ProductRepository repo;

    @PostMapping("/api/products")
    ResponseEntity saveProduct(@RequestBody @Valid CreateProductRequest productRequest) {
        Product product =
                repo.save(
                        new Product(
                                productRequest.getName(),
                                productRequest.getPrice(),
                                productRequest.getUnit()));

        return ResponseEntity.status(201)
                .header(
                        "location",
                        String.format("http://localhost/api/products/%d", product.getId()))
                .build();
    }

    @GetMapping("/api/products/{id}")
    ResponseEntity getProduct(@PathVariable Long id) {
        Product product = repo.findById(id).orElseThrow(IllegalArgumentException::new);
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(product);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity handleIllegalException(Exception e) {
        return ResponseEntity.status(404).build();
    }
}
