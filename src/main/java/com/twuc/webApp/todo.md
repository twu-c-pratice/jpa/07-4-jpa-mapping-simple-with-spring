# TODO

这个 Web API 提供了 Product 的创建和查询功能。其中有两个 API：

## 创建 Product

| Key                   | Description                                                  |
|-----------------------|--------------------------------------------------------------|
| URI                   | `/api/products`                                              |
| Method                | POST                                                         |
| Request Content\-Type | `application/json`                                           |
| Request Content       | `{"name":"<name>", "price":<price integer>,"unit":"<unit>"}` |
| Response Status       | `201`                                                        |
| Response Header       | `location:<host>/api/products/<product id>`                  |

其中，Request Content 要求如下：

| Field | Requirement                     |
|-------|---------------------------------|
| name  | 不能为 `null` 或者空字符串，最大长度为 64 字符。 |
| price | 不能为 `null`，最小值为 1 最大值为 10000   |
| unit  | 不能为 `null` 或者空字符串，最大长度为 32 字符。 |

如果 Request Content 不能够满足要求，该 API 的 Response Status 应当为 400。

## 查询 Product

| Key              | Description                                                  |
|------------------|--------------------------------------------------------------|
| URI              | `/api/products/{productId}`                                  |
| Method           | `GET`                                                        |
| Response Status  | `200`                                                        |
| Response Content | `{"name":"<name>", "price":<price integer>,"unit":"<unit>"}` |

如果指定的 Product 不存在，则 Response Status 为 `404`。

# 要求

当你拿到了这个题目，一运行测试发现，太惨了，全都是挂的。但是你看这个练习里面根本没有标记 TODO 的范围。怎么办呢？那就自己找找吧。看看如何实现才能够确保 API 的功能呢？

除了这个文档你也可以尝试观察一下测试。相信会对你有一定的帮助。