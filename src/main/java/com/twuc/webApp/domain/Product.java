package com.twuc.webApp.domain;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity
public class Product {
    @Id @GeneratedValue private Long id;

    @Column(nullable = false, length = 64)
    @Min(1)
    private String name;

    @Column(nullable = false)
    @Size(min = 1, max = 10000)
    private Integer price;

    @Column(nullable = false, length = 32)
    @Min(1)
    private String unit;

    public Product() {}

    public Product(String name, @Size(min = 1, max = 10000) Integer price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}
